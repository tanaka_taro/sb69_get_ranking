

import glob
from datetime import datetime
from matplotlib import pyplot as plt
from matplotlib.font_manager import FontProperties


def load_file_lines(file_name):
    result = []
    with open(file_name, "r", encoding="utf8", errors="ignore") as f:
        for i in f.readlines():
            i = i.replace("\n", "")
            i = i.replace("\r", "")
            result.append(i)
    return result


class Player:
    def __init__(self):
        self.id = 0
        self.name = ""
        self.scores = []  # list float

    def __repr__(self):
        scr = "(" + ", ".join(map(str, self.scores)) + ")"
        return "(" + ", ".join([self.id, self.name, scr]) + ")"


class Ranking:
    def __init__(self):
        self.load_log_format = "txt/*0.txt"
        self.file_date_format = "txt/%Y_%m_%d-%H_%M.txt"  # ファイル名->日時のフォーマット
        self.label_date_format = "%m/%d %H:%M"  # x軸に表示する日時のフォーマット

        self.font_path = r"C:\WINDOWS\Fonts\msgothic.ttc"

        self.max_show_log = 720
        self.top_numbers = 9  # 上位の何人を表示するか
        self.log_list_all = []
        self.log_list_use = []
        self.view_player_list = []  # tuple (id, name)
        self.scores = {}  # dict {id:Player()}
        self.x_interval = 1  # 目盛りの間隔
        self.avg_graph_hours = 3  # 平均用のグラフの単位時間

        self.load_list()
        self.set_view_player()
        self.load_scores()

    def load_list(self):
        self.log_list_all = glob.glob(self.load_log_format)
        for i in range(len(self.log_list_all)):
            self.log_list_all[i] = self.log_list_all[i].replace("\\", "/")
        self.log_list_use = self.log_list_all[-self.max_show_log:]

    def set_view_player(self):
        lines = load_file_lines(self.log_list_use[-1])
        for i in lines[0:self.top_numbers]:
            i_l = i.split("\t")
            p = Player()
            p.id = i_l[1]
            p.name = i_l[2]
            self.scores[p.id] = p
            self.view_player_list.append((p.id, p.name))

    def load_scores(self):
        load_count = 0
        for f_name in self.log_list_use:
            load_count += 1
            lines = load_file_lines(f_name)

            for j in lines:  # スコアをリストに追加
                j_s = j.split("\t")
                if j_s[1] in self.scores:
                    print(j_s[3])
                    self.scores[j_s[1]].scores.append(float(j_s[3]))

            for key, j in self.scores.items():  # 追加されていなければNanを追加
                if len(j.scores) != load_count:
                    j.scores.append(float("nan"))

    def show_total_graph(self):
        font = FontProperties(fname=self.font_path)

        for p_id, p_name in self.view_player_list:
            plt.plot(self.scores[p_id].scores, label=p_name)

        plt.legend(loc="upper left", prop=font)
        time_label = self.log_list_use[0::self.x_interval]
        for i in range(len(time_label)):
            time_label[i] = datetime.strptime(
                time_label[i], self.file_date_format) \
                .strftime(self.label_date_format)

        plt.xticks(
            range(0, min([self.max_show_log, len(self.log_list_use)]), self.x_interval),
            time_label, rotation=90)

        plt.grid()
        plt.show()

    def show_sub_graph(self):  # １時間毎の差のグラフ
        font = FontProperties(fname=self.font_path)

        sub_scores = {}
        for key, i in self.scores.items():
            sub_scores[i.id] = []
            for j in range(1, len(i.scores)):
                sub_scores[i.id].append(i.scores[j] - i.scores[j-1])

        for p_id, p_name in self.view_player_list:
            plt.plot(sub_scores[p_id], label=p_name)

        plt.legend(loc="upper left", prop=font)
        time_label = self.log_list_use[0::self.x_interval]
        for i in range(len(time_label)):
            time_label[i] = datetime.strptime(
                time_label[i], self.file_date_format) \
                .strftime(self.label_date_format)
        plt.xticks(
            range(0, min([self.max_show_log, len(self.log_list_use)]), self.x_interval),
            time_label, rotation=90)

        plt.grid()
        plt.show()

    def show_avg_graph(self):
        font = FontProperties(fname=self.font_path)

        avg_scores = {}
        for key, i in self.scores.items():
            avg_scores[i.id] = []
            for j in range(1, len(i.scores)-self.avg_graph_hours, self.avg_graph_hours):
                score_first = i.scores[j]
                score_last = i.scores[j+self.avg_graph_hours]
                avg_scores[i.id].append((score_last-score_first) / self.avg_graph_hours)

        for p_id, p_name in self.view_player_list:
            plt.plot(avg_scores[p_id], label=p_name)

        plt.legend(loc="upper left", prop=font)
        time_label = self.log_list_use[0::self.x_interval*self.avg_graph_hours]
        for i in range(len(time_label)):
            time_label[i] = datetime.strptime(
                time_label[i], self.file_date_format) \
                .strftime(self.label_date_format)
        plt.xticks(
            range(0,
                  min([self.max_show_log, len(self.log_list_use) // self.avg_graph_hours]),
                  self.avg_graph_hours),
            time_label, rotation=90)
        plt.title(str(self.avg_graph_hours) + "hours average")

        plt.grid()
        plt.show()


r = Ranking()

r.show_total_graph()
r.show_sub_graph()
r.show_avg_graph()
