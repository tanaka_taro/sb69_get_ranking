# -*- coding: utf-8 -*-

import os
import json
import urllib.request
from datetime import datetime, timedelta
from time import sleep


save_directory = "ranking20171124"

auth_token = "d98b1838f94fe7409bc1083f2dd42837"
event_key = "ALL:EVENT_ID:EVENT_TYPE:3005:26"

user_id = "15067436775769"

get_data_per_hour = 4


def get_json() -> str:
    url = "http://sb69.geechs-app.com/1/Ranking/getRanking"
    query = {
        "ranking_key": event_key,
        "owner_user_id": user_id,
        }

    headers = {
        "SBR-AUTHORIZED-TOKEN": auth_token,  # 重要
        "SBR-API-VERSION": "4.1.4",
    }

    try:
        print("取得")
        data = urllib.parse.urlencode(query)
        data = data.encode('ascii')

        req = urllib.request.Request(url, data, headers)
        with urllib.request.urlopen(req) as response:
            json_str = response.read().decode("utf-8")
    except:
        print("取得失敗")

    return json_str


def save_json(json_str: str, file_name: str):
    js = json.loads(json_str)

    result = ""
    for i in js["action"]["ranking_data"]["user_ranking"]:
        a = [i["rank"], i["user_id"], i["nickname"], i["total_max_score"]]
        a = map(str, a)
        result += "\t".join(a) + "\n"

    result = result.encode("UTF-8", "ignore")
    result = result.decode("utf-8", "ignore")

    print("保存", file_name)
    try:
        with open(file_name, "w", encoding="utf-8", errors="ignore") as f:
            f.write(result)
        print("保存完了")
    except:
        print("保存失敗")


def main():
    if not os.path.isdir(save_directory):
        os.mkdir(save_directory)
        print("フォルダ作成:", save_directory)

    while True:
        dt = datetime.now()
        dt_str = dt.strftime("%Y_%m_%d-%H_%M")
        save_file = save_directory + "/" + dt_str + ".txt"

        js = get_json()
        save_json(js, save_file)

        # 次回の時刻
        last_hour_part = dt.minute // get_data_per_hour

        next_minute = (dt.minute//(60//get_data_per_hour) + 1) * (60//get_data_per_hour)

        next_date = datetime(dt.year, dt.month, dt.day, dt.hour, 0) + timedelta(minutes=next_minute)
        if next_minute <= dt.minute:
            next_date += timedelta(hours=1)

        print("次回取得:", next_date.strftime("%Y_%m_%d-%H_%M"))
        print("-------\n")

        while True:
            if datetime.now() >= next_date:
                break
            else:
                sleep(20)  # 秒


if __name__ == "__main__":
    main()
